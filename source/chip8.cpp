#include "memory.hpp"
#include "opcode.hpp"
#include "registers.hpp"
#include <fstream>
#include <iostream>
#include <cstdlib>
#include <stdio.h>

static std::vector<char> ReadAllBytes(char const *filename)
{
    std::ifstream ifs(filename, std::ios::binary | std::ios::ate);
    std::ifstream::pos_type pos = ifs.tellg();

    std::vector<char> result(pos);

    ifs.seekg(0, std::ios::beg);
    ifs.read(&result[0], pos);

    return result;
}

int main(int argv, const char * argc[])
{

    if(argv < 2)
    {
        std::cout << "Input format: chip8 program_name" << std::endl;
        exit(EXIT_FAILURE);
    }

    // std::ifstream input(argc[1], std::ios::binary);     //open program in binary mode

    std::vector<char> program = ReadAllBytes(argc[1]);

    for (size_t i = 1; i < program.size(); ++i)
    {
        printf("%04d\t%02X%02X\t", (unsigned int)i, (unsigned char)program[i - 1], (unsigned char)program[i]);
        std::cout << opcode_to_string(decode_opcode(program[i - 1], program[i])) << std::endl;
    }

    //initialize memory
    memoryMap memory_map;
    Registers reg;

    reg.printRegisters();
    reg.printStack();


}