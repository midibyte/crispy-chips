#ifndef REGISTERS_H
#define REGISTERS_H

#include <stack>
#include <stdio.h>

class Registers
{
    private:
        unsigned char V[0x10];              //16 registers 8bits each do not use register VF
        // std::vector<unsigned char> V;      //16 registers 8bits each do not use register VF
        std::stack<unsigned int> stack;   //stack is 16 16bit entries
        unsigned int I;                   //I register
        unsigned int PC;                  //Program Counter
        unsigned int SP;                  //Stack Pointer
        unsigned char sound_timer;
        unsigned char delay_timer;

    public:
        //set everything to zero
        Registers()
        {
            for(int i = 0; i < 0x10; ++i)
                V[i] = 0;
            I = 0;
            PC = 0;
            SP = 0;
            sound_timer = 0;
            delay_timer = 0;
        }
        void setV(int num, unsigned int val)
        {
            V[num] = val;
        }
        unsigned char getV(int num)
        {
            return V[num];
        }
        void stackPush(unsigned int val)
        {
            stack.push(val);
        }
        unsigned int stackTop()
        {
            return stack.top();
        }

        void setI(unsigned int val)
        {
            I = val;
        }
        unsigned int getI(unsigned int val)
        {
            return I;
        }
        void setPC(unsigned int val)
        {
            PC = val;
        }
        unsigned int getPC(unsigned int val)
        {
            return PC;
        }
        void setSP(unsigned int val)
        {
            SP = val;
        }
        unsigned int getSP(unsigned int val)
        {
            return SP;
        }

        void printStack()
        {
            printf("\nStack: ");
            std::stack<unsigned int> tempStack(stack);
            int i = 0;
            while(!tempStack.empty())
            {
                printf("%02d  %04X", i++, stack.top());
                stack.pop();
            }
            printf("\n");
        }
        void printRegisters()
        {
            printf("\nRegisters: ");
            for(int i = 0; i < 0x10; ++i)
            {
                printf("V%X  %02X\t", i, V[i]);
            }
            printf("\n");
        }

};

#endif