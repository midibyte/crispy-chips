#ifndef MEMORY_H
#define MEMORY_H

#include <vector>


//chip8 memory and functions
//(0xF00-0xFFF) display

class memoryMap
{
    private:
        std::vector<unsigned char> memory_map; 
        int memorySize;
    public:
        memoryMap ()
        {
            memorySize = 0x1000;
            memory_map.resize(4000, 0); //initialize memory to all zeroes -- 4KB
        }

        unsigned char getByte(size_t index)   //returns a byte from memory
        {
            return memory_map.at(index);
        }

        int setByte(size_t index, unsigned char value)
        {
            if(index + 1 >= memorySize || index < 0x200)    //system memory is from 0x0 to 0x200
                return -1;

            memory_map[index] = value;
            return 1;
        }
};


#endif