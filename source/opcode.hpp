#ifndef OPCODE_H
#define OPCODE_H

#include <string>
#include <vector>

//reduces an opcode to 4 4bit pieces
std::vector<unsigned char> separate_opcode(unsigned char top, unsigned char btm)
{
    // unsigned char opcode[4];
    std::vector<unsigned char> opcode(4);
    opcode[0] = top >> 4;
    opcode[1] = top - (opcode[0] << 4);
    opcode[2] = btm >> 4;
    opcode[3] = btm - (opcode[2] << 4);

    return opcode;

}

//returns info about an opcode
std::string opcode_to_string(unsigned char opcode)
{
    //define info strings for opcodes in order
    if(opcode == 0xFF || opcode > 0x21 || opcode < 0x00)
        return "UNDEFINED";
    std::vector<std::string> info{
        "clear screen",                             //0
        "return from subroutine",                   //1
        "goto NNN",                                 //2
        "call subroutine at NNN",                   //3
        "if(Vx==NN)",                               //4
        "if(Vx!=NN)",                               //5
        "if(Vx==Vy)",                               //6
        "Vx = NN",                                  //7
        "Vx += NN",                                 //8
        "Vx = Vy",                                  //9
        "Vx = Vx OR Vy",                            //A
        "Vx = Vx AND Vy",                           //B
        "Vx = Vx XOR Vy",                           //C
        "Vx += Vy",                                 //D
        "Vx -= Vy",                                 //E
        "Vx = Vy = Vy >> 1",                        //F
        "Vx = Vy - Vx",                             //10
        "Vx = Vy = Vy << 1",                        //11
        "if(Vx!=Vy)",                               //12
        "I = NNN",                                  //13
        "PC = V0 + NNN",                            //14
        "Vx = rand() & NN",                         //15
        "draw at Vx, Vy, Height N, from addr in I", //16
        "skips next ins if key Vx is pressed",      //17
        "skips next ins if key Vx is not pressed",  //18
        "Vx = delay_timer",                         //19
        "Vx = sound_timer",                         //1A
        "delay_timer = Vx",                         //1B
        "sound_timer = Vx",                         //1C
        "I += Vx",                                  //1D
        "I = sprite_addr[Vx]",                      //1E
        "BCD",                                      //1F
        "store V0 to Vx starting at addr I",        //20
        "fill V0 to Vx from I"                      //21
    };

    return info[opcode];
}

//decodes the opcode a number representing the operation
unsigned char decode_opcode(unsigned char top, unsigned char btm)
{
    std::vector<unsigned char> opcode = separate_opcode(top, btm);

    printf("opcode: %X %X %X %X\t\t", opcode[0], opcode[1], opcode[2], opcode[3]);

    //take an instruction and convert it to a string
    switch(opcode[0]) {  //first check top 4 bits for type
        case 0x0:
            if(opcode[1] == 0x0 && opcode[2] == 0xE && opcode[3] == 0x0)
                return 0xFF;    //usually unused
            else if (opcode[1] == 0x0 && opcode[2] == 0xE && opcode[3] == 0xE)
                return 0x0;     //clear display
            else
                return 0x1;     //return from a subroutine
            break;
        case 0x1:
            return 0x2;         //goto NNN
            break;
        case 0x2:
            return 0x3;
            break;
        case 0x3:
            return 0x4;
            break;
        case 0x4:
            return 0x5;
            break;
        case 0x5:
            return 0x6;
            break;
        case 0x6:
            return 0x7;
            break;
        case 0x7:
            return 0x8;
            break;
        case 0x8:
            switch(opcode[3])
            {
                case 0x0:
                    return 0x9;
                    break;
                case 0x1:
                    return 0xA;
                    break;
                case 0x2:
                    return 0xB;
                    break;
                case 0x3:
                    return 0xC;
                    break;
                case 0x4:
                    return 0xD;
                    break;
                case 0x5:
                    return 0xE;
                    break;
                case 0x6:
                    return 0xF;
                    break;
                case 0x7:
                    return 0x10;
                    break;
                case 0xE:
                    return 0x11;
                    break;
            }

        case 0x9:
            return 0x12;
            break;
        case 0xA:
            return 0x13;
            break;
        case 0xB:
            return 0x14;
            break;
        case 0xC:
            return 0x15;
            break;
        case 0xD:
            return 0x16;
            break;
        case 0xE:
            if(opcode[3] == 0xE)return 0x17;    //KeyOp
            else return 0x18;                   //KeyOp
            break;
        case 0xF:
            if(opcode[3] == 0x7) return 0x19;
            else if(opcode [3] == 0xA) return 0x1A;
            else if(opcode [3] == 0x5 && opcode[2] == 0x1) return 0x1B;
            else if(opcode [3] == 0x8) return 0x1C;
            else if(opcode [3] == 0xE) return 0x1D;
            else if(opcode [3] == 0x9) return 0x1E;
            else if(opcode [3] == 0x3) return 0x1F;
            else if(opcode [3] == 0x5 && opcode[2] == 0x5) return 0x20;
            else if(opcode [3] == 0x5 && opcode[2] == 0x6) return 0x21;
    }
        return 0xFF;
}




#endif